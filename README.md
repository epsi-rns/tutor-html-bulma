Bulma version of Hugo-Bootstrap Tutorial without using any static site generator.

So any beginner can use this guidance.

#  Screenshot

![screenshot][screenshot]

-- -- --

| Disclaimer | Use it at your own risk |
| ---------- | ----------------------- |

[screenshot]:         https://gitlab.com/epsi-rns/tutor-html-bulma/raw/master/html-bulma-preview.png
